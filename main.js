"use strict";

const form = document.querySelector(".password-form");

form.addEventListener("click", (event) => {
  const target = event.target;

  const password = form.querySelector("#password").value;
  const rePassword = form.querySelector("#re-password").value;

  if (target.id === "show-password") {
    const passwordToggle = target;
    const passwordField = passwordToggle.parentElement.querySelector("input");

    if (passwordToggle.classList.contains("fa-eye-slash")) {
      passwordToggle.classList.remove("fa-eye-slash");
      passwordToggle.classList.add("fa-eye");
      passwordField.type = "text";
    } else if (passwordToggle.classList.contains("fa-eye")) {
      passwordToggle.classList.remove("fa-eye");
      passwordToggle.classList.add("fa-eye-slash");
      passwordField.type = "password";
    }
    return;
  }

  document.querySelector(".input-wrapper:last-of-type").dataset.content = "";

  if (target.id === "submit") {
    event.preventDefault();

    if (password === rePassword) {
      alert("You are welcome");
    } else {
      document.querySelector(".input-wrapper:last-of-type").dataset.content =
        "Потрібно ввести однакові значення";
    }
  }
});
